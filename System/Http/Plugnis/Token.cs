using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;


namespace DNWS
{
    public class Token{

        protected static Dictionary<string,string> _tokenDictionary;
        public string genarate_token(){
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            Random random = new Random();
            return new string(Enumerable.Repeat(chars,24).Select(s => s[random.Next(s.Length)]).ToArray());
        }

    }
}
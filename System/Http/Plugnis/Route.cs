using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace DNWS
{
    public class route{

            private ArrayList _parameter = new ArrayList();
            public ArrayList parameter{
                get {return _parameter;}
            }
            private string _function;
            public string function{
                get { return _function;}
            }
            private string _plugin;

            public string plugin{
                get { return _plugin;}                
            }

            public route(string plugin,string function = "GetResponse"){
                _function = function;
                _plugin = plugin;
            }
            
    }

    public class router{

        private Dictionary<string,route> _get = new Dictionary<string,route>();
        private Dictionary<string,route> _post = new Dictionary<string,route>();
        private Dictionary<string,route> _put = new Dictionary<string,route>();
        private Dictionary<string,route> _delete = new Dictionary<string,route>();

        public route get(string method,string url){
            method = method.ToLower();
            route tmp;
            switch(method){
                case "get":
                    tmp = this.check(_get,url);
                break;
                case "post":
                    tmp = this.check(_post,url);
                break;
                case "put":
                    tmp = this.check(_put,url);
                break;
                case "delete":
                    tmp = this.check(_delete,url);
                break;   
                default:
                    tmp = null;
                break;                            
            }

            return tmp;
        }

        private route check(Dictionary<string,route> _routeBymethod,string url){

            route tmp = null;

            foreach (var _route in _routeBymethod)
            {
                if(tmp != null) continue;

                string[] route_key = Regex.Split(_route.Key, "/");
                string[] route_check = Regex.Split(url,"/");

                if(route_key.Length == route_check.Length){
                    for (int index = 0;index<route_key.Length;index++)
                    {

                        if(route_key[index] != route_check[index]){
                            if(route_key[index] == "(:num)"){
                                if(Regex.IsMatch(route_check[index], @"^\d+$") == true){
                                    if(tmp == null){
                                        tmp = _route.Value;
                                        tmp.parameter.Clear();
                                    }else{
                                        tmp = _route.Value;
                                    }
                                    tmp.parameter.Add(route_check[index]);                              
                                }
                            }else if(route_key[index] == "(:any)"){
                                if(route_check[index].Length == 0){
                                    tmp = null;
                                    break;
                                }
                                if(tmp == null){
                                    tmp = _route.Value;
                                    tmp.parameter.Clear();
                                }else{
                                    tmp = _route.Value;
                                }
                                tmp.parameter.Add(route_check[index]);
                            }else {
                                tmp = null;
                                break;
                            }
                        }else{
                            if((index+1) == route_key.Length){
                                tmp = _route.Value;
                            }
                        }
                    }
                }else{
                    continue;
                }
            }

            return tmp;

        }

        public void listen(string method,string url,string plugin,string function = "GetResponse"){

            method = method.ToLower();

            switch(method){
                case "get":
                    if(_get.ContainsKey(url) == false){
                        _get.Add(url,new route(plugin,function));
                    }
                break;
                case "post":
                    if(_post.ContainsKey(url) == false){
                        _post.Add(url,new route(plugin,function));
                    }
                break;
                case "put":
                    if(_put.ContainsKey(url) == false){
                        _put.Add(url,new route(plugin,function));
                    }
                break;
                case "delete":
                    if(_delete.ContainsKey(url) == false){
                        _delete.Add(url,new route(plugin,function));
                    }
                break;                               
            }

        }

        
    }

}
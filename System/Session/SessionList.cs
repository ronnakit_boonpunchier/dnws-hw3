using System;
using System.Collections.Generic;

namespace DNWS
{
    public class SessionList{

        protected Dictionary<string,Session> _sessionDictionary = new Dictionary<string, Session>();

        public Session Get(string key){
            if(_sessionDictionary.ContainsKey(key) == true){
                return _sessionDictionary[key];
            }else{
                Session s = new Session();
                _sessionDictionary.Add(key,s);
                return s;
            }
        }

    }
}
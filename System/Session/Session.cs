using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace DNWS
{
    public class Session{

        protected Dictionary<string,string> _sessionValue;
        
        public Session(){
            _sessionValue = new Dictionary<string, string>();
        }

        public void Set(string key,string value)
        {
            if(_sessionValue.ContainsKey(key) == false){
                _sessionValue.Add(key,value);
            }else{
                _sessionValue[key] = value;
            }           
        } 

        public string Get(string key){
            if(_sessionValue.ContainsKey(key) == true){
                return _sessionValue[key];
            }else{
                return null;
            }
        }

        public void Destroy(){
            _sessionValue = new Dictionary<string, string>();
        }

    }
}
using System.Collections.Generic;
using System;

namespace DNWS
{
    public abstract class RESTPlugin : IPlugin
    {       

        private Dictionary<string,List<string>> _outputArray = new Dictionary<string,List<string>>();
        private Dictionary<string,string> _output = new Dictionary<string,string>();

        protected Object output{
            get {
                
                Object[] output;

                if(_outputArray.Count == 0){
                   output = new Object[1];
                   output[0] = _output;
                }else if (_output.Count == 0){
                    output = new Object[1];
                    output[0] = _outputArray;
                }else{
                    output = new Object[2];
                    output[0] = _output;
                    output[1] = _outputArray;
                }
                
                return output;
            }
        }

        protected void AddOutput(string key,string value){
            if(_outputArray.ContainsKey(key) == false){
                _outputArray[key] = new List<string>();
            }else{
                _outputArray[key].Add(value);
            }  
        }

        protected void SetOutput(string key,string value){
            if(_output.ContainsKey(key) == false){
                _output.Add(key,value);
            }else{
                _output[key] = value;
            }   
        }

    }   

}
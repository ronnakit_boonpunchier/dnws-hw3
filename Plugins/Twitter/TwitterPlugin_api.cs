using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace DNWS
{
  public class TwitterApi : RESTPlugin
  {

    public override void PostProcessing()
    {

      StringBuilder sb = new StringBuilder();

      sb.Append(JsonConvert.SerializeObject(this.output));

      _response.body = Encoding.UTF8.GetBytes(sb.ToString());
     
    }

    public override void PreProcessing()
    {
      string token = _request.getRequestByKey("token");
      Token _token = new Token();

      _response = new HTTPResponse(200);
      _response.type = "application/json";

      if(token == null){
          token = _token.genarate_token();        
      }

      sessions = Program.sessionList.Get(token); 
      sessions.Set("token",token);

    }

    public void login(){

        string username = _request.getRequestByKey("username");
        string password = _request.getRequestByKey("password");

        if (username != null && password != null && username != "" && password != ""){
            if (Twitter.IsValidUser(username, password))
            {
              this.SetOutput("status","true");
              this.SetOutput("token",sessions.Get("token"));

              sessions.Set("login","true");
              sessions.Set("username",username);
               
            }else{
              this.SetOutput("status","false");
              this.SetOutput("message","worng username or password!");
              _response.status = 403;
            }
        }else{
            this.SetOutput("status","false");
            this.SetOutput("message","require username and password!");
            _response.status = 400;
        }

    }

    public void register(){

        string username = _request.getRequestByKey("username");
        string password = _request.getRequestByKey("password");

        if (username != null && password != null && username != "" && password != "")
        {
              try
              {
                Twitter.AddUser(username, password);

                this.SetOutput("status","true");
                this.SetOutput("message","User added successfully");

                this.SetOutput("token",sessions.Get("token"));

                sessions.Set("login","true");
                sessions.Set("username",username);
              }
              catch (Exception ex)
              {
                this.SetOutput("status","false");
                this.SetOutput("message",String.Format("Error adding user with error [{0}]", ex.Message));
                _response.status = 500;
              }
        }else{
            this.SetOutput("status","false");
            this.SetOutput("message","require username and password!");
            _response.status = 400;
        }

    }

    public void deleteUser(){

      if(sessions.Get("token") != null){
        if(sessions.Get("login") == "true"){
              Twitter.RemoveUser(sessions.Get("username"));
              this.SetOutput("status","true");
        }else{
            this.SetOutput("status","false");
            this.SetOutput("message","require login!");
            _response.status = 403;          
        }
      }else{
            this.SetOutput("status","false");
            this.SetOutput("message","require token!");
            _response.status = 400;
      }

    }

    public void unfollow(){

      string user = _request.getRequestByKey("username");

      if(sessions.Get("token") != null){
        if(sessions.Get("login") == "true"){

          try{
              Twitter.RemoveFollowing(sessions.Get("username"),user);
              this.SetOutput("status","true");
          }catch (Exception ex){
              this.SetOutput("status","false");
              this.SetOutput("message",String.Format("Error [{0}]", ex.Message));
              _response.status = 500;     
          }

        }else{
            this.SetOutput("status","false");
            this.SetOutput("message","require login!");
            _response.status = 403;              
        }
        
      }else{
          this.SetOutput("status","false");
          this.SetOutput("message","require token!");
          _response.status = 400;        
      }      
    }

    public void follow(){ 

      string user = _request.getRequestByKey("username");

      if(sessions.Get("token") != null){
        if(sessions.Get("login") == "true"){

          Twitter twitter = new Twitter(sessions.Get("username"));   

          Console.WriteLine(user);

          try{
              twitter.AddFollowing(user);
              this.SetOutput("status","true");
          }catch (Exception ex){
              this.SetOutput("status","false");
              this.SetOutput("message",String.Format("Error [{0}]", ex.Message));
              _response.status = 500;     
          }

        }else{
            this.SetOutput("status","false");
            this.SetOutput("message","require login!");
            _response.status = 403;              
        }
        
      }else{
          this.SetOutput("status","false");
          this.SetOutput("message","require token!");
          _response.status = 400;        
      }

    }

    public void userList(){    

      List<User> userlist = Twitter.GetList();
      int i = 1;
      foreach(User data in userlist){
          SetOutput("user number : "+i,data.Name);      
          i++;
      }

    }

    public void followList(string user)
    {

      if(user == "me"){
        if(sessions.Get("token") != null){
            if(sessions.Get("login") == "true"){
                user = sessions.Get("username");
            }else{
              this.SetOutput("status","false");
              this.SetOutput("message","require login!");
              _response.status = 403;     
              return;
            }
        }else{
          this.SetOutput("status","false");
          this.SetOutput("message","require token!");
          _response.status = 400;             
          return;
        }
      }

      SetOutput("user",user);

      List<string> userlist = Twitter.ListFollowing(user);
      int i = 1;
      try{
        foreach(string data in userlist){
            this.AddOutput("follower",data);
            i++;
        }
      }catch (Exception ex)
      {  
        this.SetOutput("message",String.Format("Error get following with error [{0}], please go back to <a href=\"/twitter\">login page</a> to try again", ex.Message));
        _response.status = 500;  
      }

    }

    public void profile(){
        SetOutput("username",sessions.Get("username"));
    }

    public void post(){
      if(sessions.Get("token") != null){
        if(sessions.Get("login") == "true"){

          Twitter twitter = new Twitter(sessions.Get("username"));   

          string message = _request.getRequestByKey("message");

          try{
              twitter.PostTweet(message);
              this.SetOutput("status","true");
          }catch (Exception ex){
              this.SetOutput("status","false");
              this.SetOutput("message",String.Format("Error [{0}]", ex.Message));
              _response.status = 500;     
          }

        }else{
            this.SetOutput("status","false");
            this.SetOutput("message","require login!");
            _response.status = 403;              
        }
        
      }else{
          this.SetOutput("status","false");
          this.SetOutput("message","require token!");
          _response.status = 400;        
      }
    }

    public void timeline(string user){

      if(user == "me"){
        if(sessions.Get("token") != null){
            if(sessions.Get("login") == "true"){
                user = sessions.Get("username");

                Twitter twitter = new Twitter(user);

                SetOutput("username",twitter.GetUsername());

                List<Tweet> tweets = twitter.GetUserTimeline();
                foreach(Tweet tweet in tweets) {
                    this.AddOutput("Message","you say : " + tweet.Message+" at " + tweet.DateCreated );
                }

                  tweets = twitter.GetFollowingTimeline();
                  if(tweets != null) {
                    foreach (Tweet tweet in tweets)
                    {
                    this.AddOutput("Message",tweet.User+" say : " + tweet.Message+" at " + tweet.DateCreated );

                    }
                  }

            }else{
              this.SetOutput("status","false");
              this.SetOutput("message","require login!");
              _response.status = 403;     
              return;
            }
        }else{
          this.SetOutput("status","false");
          this.SetOutput("message","require token!");
          _response.status = 400;             
          return;
        }
      }else{

        Twitter twitter = new Twitter(user);

        SetOutput("username",twitter.GetUsername());

        List<Tweet> tweets = twitter.GetUserTimeline();
        foreach(Tweet tweet in tweets) {
            this.AddOutput("Message",tweet.User + " say : " + tweet.Message+" at "+ tweet.DateCreated );
        }

      }

    }

    public override void GetResponse()
    {
      SetOutput("Twitter API","Running");
    }

  }
}
using System.Collections.Generic;
using System.Text;
using System;


namespace DNWS
{
  public class TwitterWeb : IPlugin
  {
    public override void PostProcessing()
    {
      throw new NotImplementedException();
    }

    public override void PreProcessing()
    {
      throw new NotImplementedException();
    }

    private StringBuilder GenTimeline(Twitter twitter, StringBuilder sb)
    {
          sb.Append("Say something<br />");
          sb.Append("<form method=\"post\">");
          sb.Append("<input type=\"text\" name=\"message\"></input>");
          sb.Append("<input type=\"submit\" name=\"action\" value=\"tweet\" /> <br />");
          sb.Append("</form>");
          sb.Append("Follow someone<br />");
          sb.Append("<form method=\"post\">");
          sb.Append("<input type=\"text\" name=\"following\"></input>");
          sb.Append("<input type=\"submit\" name=\"action\" value=\"following\" /> <br />");
          sb.Append("</form>");
          sb.Append(String.Format("<h3><b>{0}</b>'s timeline</h3><br />", twitter.GetUsername()));
          List<Tweet> tweets = twitter.GetUserTimeline();
          foreach(Tweet tweet in tweets) {
              sb.Append("[" + tweet.DateCreated + "]" + tweet.User + ":" + tweet.Message + "<br />");
          }
          sb.Append("<br /><br />");
          sb.Append("<h3>Following timeline</h3><br />");
          tweets = twitter.GetFollowingTimeline();
          if(tweets == null) {
            sb.Append("Your following list is empty, follow someone!");
          } else {
            foreach (Tweet tweet in tweets)
            {
              sb.Append("[" + tweet.DateCreated + "] " + tweet.User + ":" + tweet.Message + "<br />");
            }
          }
          return sb;
    }

    protected StringBuilder GenLoginPage(StringBuilder sb)
    {
        sb.Append("<h2>Login</h2>");
        sb.Append("<form method=\"get\">");
        sb.Append("Username: <input type=\"text\" name=\"user\" value=\"\" /> <br />");
        sb.Append("Password: <input type=\"password\" name=\"password\" value=\"\" /> <br />");
        sb.Append("<input type=\"submit\" name=\"action\" value=\"login\" /> <br />");
        sb.Append("</form>");
        sb.Append("<br /><br /><br />");
        sb.Append("<h2>New user</h2>");
        sb.Append("<form method=\"get\">");
        sb.Append("Username: <input type=\"text\" name=\"user\" value=\"\" /> <br />");
        sb.Append("Password: <input type=\"password\" name=\"password\" value=\"\" /> <br />");
        sb.Append("<input type=\"submit\" name=\"action\" value=\"newuser\" /> <br />");
        sb.Append("</form>");
        return sb;
    }


    public override void GetResponse()
    {
      _response = new HTTPResponse(200);
      StringBuilder sb = new StringBuilder();
      
      Boolean is_login = Convert.ToBoolean(HTTPProcessor.sessions.Get("is_login"));

      string user = _request.getRequestByKey("user");
      string password = _request.getRequestByKey("password");
      string action = _request.getRequestByKey("action");
      string following = _request.getRequestByKey("following");
      string message = _request.getRequestByKey("message");

              Console.WriteLine("action : "+action);

      if(is_login == true){
        user = HTTPProcessor.sessions.Get("user");
      }

      if(action == null){
        if(is_login == false) // no user? show login screen
        {        
          sb.Append("<h1>Twitter</h1>");
          sb = GenLoginPage(sb);  
        }else{

          try
          {
            Twitter twitter = new Twitter(user);
            sb.Append(String.Format("<h1>{0}'s Twitter</h1>", user));
            sb = GenTimeline(twitter, sb);
          }
          catch (Exception ex)
          {
            sb.Append(String.Format("Error [{0}], please go back to <a href=\"/twitter\">login page</a> to try again", ex.Message));
          }

        }
      }else{
         if (action.Equals("logout")){
               sb.Append("User logout successfully, please go back to <a href=\"/twitter\">login page</a> to login");
              HTTPProcessor.sessions.Destroy();

         }else if (action.Equals("newuser"))
          {
            if (user != null && password != null && user != "" && password != "")
            {
              try
              {
                Twitter.AddUser(user, password);
                sb.Append("User added successfully, please go back to <a href=\"/twitter\">login page</a> to login");
              }
              catch (Exception ex)
              {
                sb.Append(String.Format("Error adding user with error [{0}], please go back to <a href=\"/twitter\">login page</a> to try again", ex.Message));
              }
            }
          }
          else if (action.Equals("login"))
          {
            if (user != null && password != null && user != "" && password != "")
            {
              if (Twitter.IsValidUser(user, password))
              {
                HTTPProcessor.sessions.Set("user",user);
                HTTPProcessor.sessions.Set("is_login","true");
                sb.Append(String.Format("Welcome {0}, please go back to <a href=\"/twitter\">tweet page</a> to begin",user));
              }
              else
              {
                sb.Append("Error login, please go back to <a href=\"/twitter\">login page</a> to try again");
              }
            }
          }
          else
          {
            Twitter twitter = new Twitter(user);
            sb.Append(String.Format("<h1>{0}'s Twitter</h1>", user));
            if (action.Equals("following"))
            {
              try
              {
                twitter.AddFollowing(following);
                sb = GenTimeline(twitter, sb);
              }
              catch (Exception ex)
              {
                sb.Append(String.Format("Error [{0}], please go back to <a href=\"/twitter\">login page</a> to try again", ex.Message));
              }
            }
            else if (action.Equals("tweet"))
            {
              try
              {
                twitter.PostTweet(message);
                sb = GenTimeline(twitter, sb);
              }
              catch (Exception ex)
              {
                Console.WriteLine(ex.ToString());
                sb.Append(String.Format("Error [{0}], please go back to <a href=\"/twitter\">login page</a> to try again", ex.Message));
              }
            }
          }
      }
      
      _response.body = Encoding.UTF8.GetBytes(sb.ToString());
      
    }
  }
}
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace DNWS
{
  class Following
  {
    public int FollowingId {get; set; }
    public string Name { get; set; }
  }
  class User
  {
    public int UserId { get; set; }
    public string Name { get; set; }
    public string Password { get; set; }
    public List<Following> Following { get; set; } // Bug in SQLite implemention in EF7, no FK!
  }
  class Tweet
  {
    public int TweetId { get; set; }
    public string Message { get; set; }
    public string User { get; set; } // Bug in SQLite implemention in EF7, no FK!

    public DateTime DateCreated { get; set; }
  }
  // Ref: https://docs.microsoft.com/en-us/ef/core/get-started/netcore/new-db-sqlite
  class TweetContext : DbContext
  {
    public DbSet<Tweet> Tweets { get; set; }
    public DbSet<User> Users { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      optionsBuilder.UseSqlite("Data Source=tweet.db");
    }
  }

  class Twitter
  {
    User user;
    public Twitter(string name)
    {
      if(name == null || name == "") {
         throw new Exception("User name is required");
      }
      user = GetUser(name);
    }
    public static List<User> GetList(){
      List<User> userlist;
      using (var context = new TweetContext())
      {
        userlist = context.Users.ToList();
      }
      return userlist;
    }    
    public string GetUsername()
    {
      return user.Name;
    }
    public static void RemoveFollowing(string name, string followingName) 
    {
      if(followingName == null ) {
        throw new Exception("Following not found");
      }
      using (var context = new TweetContext())
      {
        List<User> users = context.Users.Where(b => b.Name.Equals(name)).Include(b => b.Following).ToList();
        List<Following> old_follows = users[0].Following;
        List<string> follows_name = new List<string>();
        foreach(Following n in old_follows) {
            if(!(n.Name.Equals(followingName))){ 
                follows_name.Add(n.Name); 
           }
        }
        List<Following> new_follows = new List<Following>();
        foreach(string n in follows_name) { 
            Following following = new Following();
            following.Name = n;
            new_follows.Add(following);
        }
        users[0].Following = new_follows; 
        context.Users.Update(users[0]);
        context.SaveChanges();
      }
    }
    public void AddFollowing(string followingName)
    {
      if(user == null) {
        throw new Exception("User is not set");
      }
      if(followingName == null ) {
        throw new Exception("Following not found");
      }
      using (var context = new TweetContext())
      {
        if(user.Following == null) {
          user.Following = new List<Following>();
        }
        List<Following> followings = user.Following.Where(b => b.Name == followingName).ToList();
        if(followings.Count > 0) return;
        Following following = new Following();
        following.Name = followingName;
        user.Following.Add(following);
        context.Users.Update(user);
        context.SaveChanges();
      }
    }
    public static List<string> ListFollowing(string name)
   {
      List<string> followings = new List<string>();
      using (var context = new TweetContext()) {
        List<User> users = context.Users.Where(b => b.Name.Equals(name)).Include(b => b.Following).ToList();
        List<Following> user_foll = users[0].Following;
        if(user_foll.Count == 0)
        {
          return null;
        }
        foreach(Following following in user_foll) {
          followings.Add(following.Name);
        }
      }
      return followings;
    }    
    public List<Tweet> GetTimeline(User aUser)
    {
      if(aUser == null) {
        throw new Exception("User is not set");
      }
      List<Tweet> timeline;
      using (var context = new TweetContext())
      {
          timeline = context.Tweets.Where(b => b.User.Equals(aUser.Name)).OrderBy(b => b.DateCreated).ToList();
      }
      return timeline;
    }
    public List<Tweet> GetUserTimeline()
    {
      if(user == null) {
        throw new Exception("User is not set");
      }
      return GetTimeline(user);
    }

    public List<Tweet> GetFollowingTimeline()
    {
      if(user == null) {
        throw new Exception("User is not set");
      }
      List<Tweet> timeline = new List<Tweet>();
      using (var context = new TweetContext()) {
        List<Following> followings = user.Following;
        if(followings == null || followings.Count == 0) {
          return null;
        }
        foreach(Following following in followings) {
          User followingUser = GetUser(following.Name);
          timeline.AddRange(GetTimeline(followingUser));
        }
      }
      timeline = timeline.OrderBy(b => b.DateCreated).ToList();
      return timeline;
    }
    public void PostTweet(string message)
    {
      if(user == null) {
        throw new Exception("User is not set");
      }
      Tweet tweet = new Tweet();
      tweet.User = user.Name;
      tweet.Message = message;
      tweet.DateCreated = DateTime.Now;
      using (var context = new TweetContext())
      {
        context.Tweets.Add(tweet);
        context.SaveChanges();
      }
    }
    public static void AddUser(string name, string password)
    {
      User user = new User();
      user.Name = name;
      user.Password = password;
      using (var context = new TweetContext())
      {
          List<User> userlist = context.Users.Where(b => b.Name.Equals(name)).ToList();
          if(userlist.Count > 0) {
            throw new Exception("User already exists");
          }
          context.Users.Add(user);
          context.SaveChanges();
      }
    }

    public static void RemoveUser(string name)
    {
      using (var context = new TweetContext())
      {
          List<User> users = context.Users.Where(b => b.Name.Equals(name)).Include(b => b.Following).ToList();
            Tweet tweet = new Tweet();
            List<Tweet> timeline = context.Tweets.Where(b => b.User.Equals(users[0].Name)).OrderBy(b => b.DateCreated).ToList();
            
            foreach(Tweet n in timeline){ 
              context.Tweets.Remove(n);
            }

            List<Following> followlist = users[0].Following;
            foreach(Following n in followlist) {
              Twitter.RemoveFollowing(users[0].Name, n.Name); 
            }

            List<User> userlist = context.Users.ToList();
            foreach(User n in userlist){
                Twitter.RemoveFollowing(n.Name, users[0].Name);
            }
            context.Users.Remove(users[0]);
            context.SaveChanges();
      }
    }

    public static bool IsValidUser(string name, string password)
    {
      using (var context = new TweetContext())
      {
          List<User> userlist = context.Users.Where(b => b.Name.Equals(name) && b.Password.Equals(password)).ToList();
          if(userlist.Count == 1) {
            return true;
          }
      }
      return false;
    }

    private User GetUser(string name)
    {
      using (var context = new TweetContext())
      {
        try {
          List<User> users = context.Users.Where(b => b.Name.Equals(name)).Include(b => b.Following).ToList();
          return users[0];
        } catch (Exception) {
          return null;
        }
      }
    }

  }
  
}
namespace DNWS
{
    public abstract class IPlugin
    {       
        public Session sessions = null;
        protected HTTPResponse _response;
        protected HTTPRequest _request;

        public HTTPResponse response{
            get {return _response;}
        }
        public HTTPRequest request{
            set {_request = value;}
        }
        public abstract void PreProcessing();
        public abstract void PostProcessing();
        public abstract void GetResponse();
    }   

}
using System;
using System.Text;

namespace DNWS
{
  /// <summary>
  /// Gen HTML about client info using information from request header
  /// </summary>
  /// <returns>HTML client info</returns>

  public class ClientInfoPlugin : IPlugin
  {
    public override void GetResponse()
    {
      StringBuilder sb = new StringBuilder();

      string[] client_endpoint = _request.getPropertyByKey("RemoteEndPoint").Split(':');
      string val;
      sb.Append("<html><body>");
      sb.Append("Client Port: ").Append(client_endpoint[1]).Append("<br />\n");
      if ((val = _request.getPropertyByKey("user-agent")) != null)
      {
        sb.Append("Browser Information: ").Append(val).Append("<br />\n");
      }
      if ((val = _request.getPropertyByKey("accept-language")) != null)
      {
        sb.Append("Accept Language: ").Append(val).Append("<br />\n");
      }
      if ((val = _request.getPropertyByKey("accept-encoding")) != null)
      {
        sb.Append("Accept Encoding: ").Append(val).Append("<br />\n");
      }
      sb.Append("</body></html>");

      _response = new HTTPResponse(200);
      _response.body = Encoding.UTF8.GetBytes(sb.ToString());

    }

    public override void PostProcessing()
    {
      throw new NotImplementedException();
    }

    public override void PreProcessing()
    {
      throw new NotImplementedException();
    }
  }
}
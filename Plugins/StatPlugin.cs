using System;
using System.Collections.Generic;
using System.Text;

namespace DNWS
{
  public class StatPlugin : IPlugin
  {
    
    protected static Dictionary<String, int> statDictionary = null;
    public StatPlugin()
    {
      if (statDictionary == null)
      {
        statDictionary = new Dictionary<String, int>();

      }
    }

    public override void PreProcessing()
    {
      if (statDictionary.ContainsKey(_request.Url))
      {
        statDictionary[_request.Url] = (int)statDictionary[_request.Url] + 1;
      }
      else
      {
        statDictionary[_request.Url] = 1;
      }
    }
    public override void GetResponse()
    {
      StringBuilder sb = new StringBuilder();
      sb.Append("<html><body><h1>Stat:</h1>");
      foreach (KeyValuePair<String, int> entry in statDictionary)
      {
        sb.Append(entry.Key + ": " + entry.Value.ToString() + "<br />");
      }
      sb.Append("</body></html>");
      _response = new HTTPResponse(200);
      _response.body = Encoding.UTF8.GetBytes(sb.ToString());
    }

    public override void PostProcessing()
    {
      throw new NotImplementedException();
    }
  }
}